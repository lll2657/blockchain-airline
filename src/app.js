var web3Provider = null;
var AirlineContract;
const nullAddress = "0x0000000000000000000000000000000000000000";

function init() {
  // We init web3 so we have access to the blockchain
  initWeb3();
}

function initWeb3() {
  if (typeof web3 !== 'undefined' && typeof web3.currentProvider !== 'undefined') {
    web3Provider = web3.currentProvider;
    web3 = new Web3(web3Provider);
  } else {    
    console.error('No web3 provider found. Please install Metamask on your browser.');
    alert('No web3 provider found. Please install Metamask on your browser.');
  }
  
  initAirlineContract();
}

function initAirlineContract () {
  $.getJSON('Airline.json', function(data) {
    // Get the necessary contract artifact file and instantiate it with truffle-contract
    AirlineContract = TruffleContract(data);

    // Set the provider for our contract
    AirlineContract.setProvider(web3Provider);

    // listen to the events emitted by our smart contract
    getEvents ();
  });
}

function getEvents () {
  AirlineContract.deployed().then(function(instance) {
  var events = instance.allEvents(function(error, log){
    if (!error){
    	if(log.event == "DelayEvent"){
    		$("#eventsList").prepend('<li>' + log.event + ': Flight ' + log.args.flight + ' delayed for ' + log.args.delayedHours.c[0] 
    			+ ' hours and ' + log.args.delayedMinutes.c[0] + ' minutes</li>');
    	}
    	else if(log.event == "PayoutEvent"){
    		$("#eventsList").prepend('<li>' + log.event + ': Customer ' + log.args.customer + ' is paid ' + log.args.payout.c[0]/10000 
    			+ ' ether</li>');
    	}
    }
  });
  }).catch(function(err) {
    console.log(err.message);
  });
}

function updateBalance() {
  AirlineContract.deployed().then(function(instance) {
  	web3.eth.getBalance(instance.address, function(error, result){
  		if(!error)
        	$("#balance").text(web3.fromWei(result, 'ether'))
	    else
	        console.error(error);
  	});
  }).catch(function(err) {
    console.log(err.message);
  });
}

function addToContract () {
  var value = $("#etherToContract").val();
  web3.eth.getAccounts(function(error, accounts) {
  if (error) {
    console.log(error);
  } else {
    if(accounts.length <= 0) {
      alert("No account is unlocked, please authorize an account on Metamask.")
    } else {
      AirlineContract.deployed().then(function(instance) {
        return instance.addValToContract({from: accounts[0], value: web3.toWei(value,"ether")});
      }).then(function(result) {
        console.log('Added ' + value + ' ether to balance');
        updateBalance();
      }).catch(function(err) {
        console.log(err.message);
      });
    }
  }
  });
}

function registerAirline() {
  web3.eth.getAccounts(function(error, accounts) {
  if (error) {
    console.log(error);
  } else {
    if(accounts.length <= 0) {
      alert("No account is unlocked, please authorize an account on Metamask.")
    } else {
      AirlineContract.deployed().then(function(instance) {
        return instance.registerAsAirline({from: accounts[0]});
      }).then(function(result) {
        console.log('Registered as Airline');
      }).catch(function(err) {
        console.log(err.message);
      });
    }
  }
  });
}

function airlineProvideInfo() {
  var flightID = $("#flightID").val();
  var hour = $("#hour").val();
  var minute = $("#minute").val();
  web3.eth.getAccounts(function(error, accounts) {
  if (error) {
    console.log(error);
  } else {
    if(accounts.length <= 0) {
      alert("No account is unlocked, please authorize an account on Metamask.")
    } else {
      AirlineContract.deployed().then(function(instance) {
        return instance.airlineProvideInfo(flightID, hour, minute, {from: accounts[0]});
      }).then(function(result) {
      	console.log(result);
        console.log('Report Successful');
        $("#airlineList").prepend('<li>Flight Number: '+flightID+' Time: '+hour+':'+minute+'</li>');
        updateBalance();
      }).catch(function(err) {
        console.log(err.message);
      });
    }
  }
  });
}

function buyPlan() {
  var plan = $("#plan").val();
  var flightID = $("#customerFlightID").val();
  var hour = $("#customerHour").val();
  var minute = $("#customerMinute").val();
  web3.eth.getAccounts(function(error, accounts) {
  if (error) {
    console.log(error);
  } else {
    if(accounts.length <= 0) {
      alert("No account is unlocked, please authorize an account on Metamask.")
    } else {
      AirlineContract.deployed().then(function(instance) {
        return instance.buyPlan(flightID, hour, minute, {from: accounts[0], value: web3.toWei(plan,"ether")});
      }).then(function(result) {
        console.log('Plan ' + plan + ' Purchased');
        $("#customerList").prepend('<li>CustomerID: '+accounts[0]+' Plan: '+plan+' Flight Number: '+flightID+' Time: '+hour+':'+minute+'</li>');
        updateBalance();
      }).catch(function(err) {
        console.log(err.message);
      });
    }
  }
  });
}

// When the page loads, this will call the init() function
$(function() {
  $(window).load(function() {
    init();
  });
});