pragma solidity ^0.4.18;

contract Airline {

	address[] public customer;
	address public insurance;
	address public airline;
    
	uint[] plan;
	string[] flight;
	uint[] hour;
	uint[] minute;
	bool[] paid;

    /**
    * The logs that will be emitted in every step of the contract's life cycle
    */
	event PlanBoughtEvent(uint plan, string flight, uint hour, uint minute);
	event DelayEvent(string flight, uint delayedHours, uint delayedMinutes);
	event PayoutEvent(address customer, uint plan, uint payout);

    /**
    * The contract constructor
    */
	constructor() public payable{
		insurance = msg.sender;
	}
	
	function addValToContract() public payable {
	    require(insurance == msg.sender);
	}

	function getBalance() public returns(uint){
		return this.balance;
	}
	
	function registerAsAirline() public {
	    require(airline == address(0) && msg.sender != insurance);
	    
	    airline = msg.sender;
	}
	
	function buyPlan(string _flight, uint _hour, uint _minute) public payable {
	    require(airline != msg.sender && insurance != msg.sender);
	    
	    if(msg.value == 1 ether){
	        createPlan(msg.sender, 1, _flight, _hour, _minute);
	    } else if(msg.value == 2 ether){
	        createPlan(msg.sender, 2, _flight, _hour, _minute);
	    } else if(msg.value == 3 ether){
	        createPlan(msg.sender, 3, _flight, _hour, _minute);
	    }
	}
	
	function createPlan(address _customer, uint _plan, string _flight, uint _hour, uint _minute) internal {
	    customer.push(_customer);
	    plan.push(_plan);
	    flight.push(_flight);
	    hour.push(_hour);
	    minute.push(_minute);
	    paid.push(false);
	    
	    emit PlanBoughtEvent(_plan, _flight, _hour, _minute);
	}

    function airlineProvideInfo(string _flight, uint _hour, uint _minute) public{
        require(airline == msg.sender);
        
        for (uint i=0; i<flight.length; i++) {
            if(keccak256(flight[i]) == keccak256(_flight)){
                if(hour[i] != _hour || minute[i] != _minute){
                    uint delayedHours = _hour - hour[i];
                    uint delayedMinutes = _minute - minute[i];
                    emit DelayEvent(_flight, delayedHours, delayedMinutes);
                    
                    uint amount = (1 ether * delayedHours + 0.5 ether * delayedMinutes) * plan[i];
                    
                    payout(i, amount);
                }
            }
        }
        
        payAirline();
    }
    
    function payout(uint _index, uint _amount) internal{
        require(paid[_index] == false);
        
        customer[_index].transfer(_amount);
        
        paid[_index] = true;
        
        emit PayoutEvent(customer[_index], plan[_index], _amount);
    }
    
    function payAirline() internal {
        airline.transfer(0.5 ether);
    }
}