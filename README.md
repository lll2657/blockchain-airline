1. Install Metamask on google chroom as a plugin: https://metamask.io/
2. Download Ganache https://truffleframework.com/ganache/ and host it using command 
	 ganache-cli -p [port number]
3. Delete all the files in airlines/build/contracts (i.e. all the json files)
4. Configure the settings in airlines/truffle-config.js (Currently, it is 127.0.0.1:7545, needs to be same port as Ganache) 
5. Run the commands to compile and deploy:
	truffle compile
	truffle migrate --network development
6. Run the command to start the web app:
	npm run dev
7. Click on Metamask, then click on the top left of the pop-up, choose the correct network (same as Ganache). If there is no such option, click on the “Custom RPC” option, and add that url, so Metamask can connect with Ganache.
8. Click on the “restore from seed phrase” option shown in the pop up of Metamask, add copy-past the 12 words of the mnemonic on Ganache, and create any password. This unlocks the first account in Ganache.
9. Add other accounts of Ganache by clicking on the user icon on the top right of Metamask, and choose “import account”, paste the private key that you can copy from the ganache-cli.